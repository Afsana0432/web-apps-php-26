<?php
include_once "../../src/GenderSelection/GenderSelection.php";

$storeObject = new GenderSelection();

$storeObject->prepare($_POST);

if (!empty($_POST["name"]) && !empty($_POST["gender"])) {
    $storeObject->store();
} else {
    echo "Unable to submit. Please try again!";
}

?>