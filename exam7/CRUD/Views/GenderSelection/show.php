<?php
include_once "../../src/GenderSelection/GenderSelection.php";

$showObject = new GenderSelection();

$onedata = $showObject->prepare($_GET)->show();

//echo "<pre>";
//print_r($onedata);

?>
<html>
<head>
    <title>Edit | Show</title>
</head>
<body>
<a href="create.php">Back to add page</a>
<a href="index.php">Back to list page</a>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Gender</th>
    </tr>
    <td><?php echo $onedata['id']?></td>
    <td><?php echo $onedata['title']?></td>
    <td><?php echo $onedata['gender']?></td>
</table>
</body>
</html>

