<?php
include_once "../../src/GenderSelection/GenderSelection.php";

$editObject = new GenderSelection();

$singledata = $editObject->prepare($_GET)->show();
//
//echo "<pre>";
//print_r($singledata);

if (isset($_SESSION["message"]) && !empty($_SESSION["message"])) {
    echo $_SESSION["message"];
    unset($_SESSION["message"]);
}

?>


<html>
<head>
    <title>Edit Page</title>
</head>
<body>
<a href="index.php">See list</a>

<fieldset style="width:30%; margin-top: 50px">
    <legend>Update Gender Selection</legend>
    <form action="update.php" method="post">
        <label>Name: </label>
        <input type="text" name="name" placeholder="Enter your name" value="<?php echo $singledata["title"] ?>"><br><br>
        <label>Update your gender: </label>
        <label for="male">
            <input type="radio" name="gender"
                   value="Male" <?php echo ($singledata["gender"] == 'Male') ? 'checked' : '' ?> id="male">
            Male
        </label>
        <label for="female">
            <input type="radio" name="gender"
                   value="Female" <?php echo ($singledata["gender"] == 'Female') ? 'checked' : '' ?> id="female">
            Female
        </label><br><br>
        <input type="hidden" name="id" value="<?php echo $_GET["id"] ?>">
        <input type="submit" value="Update">
    </form>
</fieldset>

</body>
</html>
