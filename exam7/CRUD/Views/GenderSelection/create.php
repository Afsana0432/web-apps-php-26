<?php

session_start();

if (isset($_SESSION["message"]) && !empty($_SESSION["message"])) {
    echo $_SESSION["message"];
    unset($_SESSION["message"]);
}

?>

<html>
<head>
    <title>Add Page</title>
</head>
<body>
<a href="index.php">See list</a>

<fieldset style="width:30%; margin-top: 50px">
    <legend>Gender Selection</legend>
    <form action="store.php" method="post">
        <label>Name: </label>
        <input type="text" name="name" placeholder="Enter your name"><br><br>
        <label>Select your gender: </label>
        <label for="male">
            <input type="radio" name="gender" value="Male" id="male">
            Male
        </label>
        <label for="female">
            <input type="radio" name="gender" value="Female" id="female">
            Female
        </label><br><br>
        <input type="submit">
    </form>
</fieldset>

</body>
</html>

