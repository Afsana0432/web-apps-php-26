<?php
include_once "../../src/GenderSelection/GenderSelection.php";

$listObject = new GenderSelection();

$alldata = $listObject->index();

if (isset($_SESSION["message"]) && !empty($_SESSION["message"])) {
    echo $_SESSION["message"];
    unset($_SESSION["message"]);
}

//echo "<pre>";
//print_r($alldata);

?>
<html>
<head>
    <title>Index | List</title>
</head>
<body>
<a href="create.php">Back to add page</a>
<table border="1">
    <tr>
        <th>SL</th>
        <th>Name</th>
        <th>Gender</th>
        <th colspan="3">Action</th>
    </tr>
    <?php
    $serial = 1;
    if (isset($alldata) && !empty($alldata)) {
        foreach ($alldata as $onedata) {
            ?>

            <tr>
                <td><?php echo $serial++ ?></td>
                <td><?php echo $onedata["title"] ?></td>
                <td><?php echo $onedata["gender"] ?></td>
                <td><a href="show.php?id=<?php echo $onedata["id"] ?>">View</a></td>
                <td><a href="edit.php?id=<?php echo $onedata["id"] ?>">Edit</a></td>
                <td><a href="delete.php?id=<?php echo $onedata["id"] ?>">Delete</a></td>
            </tr>

        <?php }
    } else { ?>
        <tr>
            <td colspan="4">No available Data</td>
        </tr>
    <?php } ?>

</table>
</body>
</html>

