<?php

class GenderSelection
{
    public $id = "";
    public $title = "";
    public $gender = "";
    public $data = "";

    public function prepare($data = "")
    {
        if (array_key_exists("id", $data)) {
            $this->id = $data["id"];
        }
        if (array_key_exists("name", $data)) {
            $this->title = $data["name"];
        }
        if (array_key_exists("gender", $data)) {
            $this->gender = $data["gender"];
        }
        return $this;
    }

    public function __construct()
    {
        session_start();
        $conn = mysql_connect("localhost", "root", "") or die('Opps! Unable to connect with Mysql');
        mysql_select_db("php26") or die('Unable to connect with Database');
    }

    public function store()
    {
        $query = "INSERT INTO `gender_selection` (`id`, `title`, `gender`) VALUES (NULL, '$this->title', '$this->gender')";
        if (mysql_query($query)) {
            $_SESSION["message"] = "Data Successfully Submitted";
        }
        header("location:create.php");
    }

    public function index()
    {
        $query = "SELECT * FROM `gender_selection`";
        $mydata = mysql_query($query);
        while ($row = mysql_fetch_assoc($mydata)) {
            $this->data[] = $row;
        }
        return $this->data;
    }

    public function show()
    {
        $query = "SELECT * FROM `gender_selection` WHERE id=" . $this->id;
        $mydata = mysql_query($query);
        $row = mysql_fetch_assoc($mydata);
        return $row;
    }

    public function update()
    {
        $query = "UPDATE `gender_selection` SET `title` = '$this->title', `gender` = '$this->gender' WHERE
        `gender_selection`.`id` =" . $this->id;
        if (mysql_query($query)) {
            $_SESSION["message"] = "Data Successfully Updated";
        }
        header("location:edit.php?id=$this->id");
    }

    public function delete(){
        $query = "DELETE FROM `gender_selection` WHERE `gender_selection`.`id` =".$this->id;
        if (mysql_query($query)){
            $_SESSION["message"] = "Data Successfully Deleted";
        }
        header("location:index.php");
    }


}