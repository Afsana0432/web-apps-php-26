-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2016 at 06:16 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `php26`
--

-- --------------------------------------------------------

--
-- Table structure for table `gender_selection`
--

CREATE TABLE IF NOT EXISTS `gender_selection` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `unique_id` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender_selection`
--

INSERT INTO `gender_selection` (`id`, `title`, `gender`, `unique_id`) VALUES
(9, 'Arafat', 'Male', ''),
(10, 'themeyellow', 'Male', ''),
(11, 'MD Iyasin Arafat', 'Male', ''),
(12, 'BDH', 'Female', ''),
(13, 'et4ert', 'Male', '57846e963230c');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gender_selection`
--
ALTER TABLE `gender_selection`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gender_selection`
--
ALTER TABLE `gender_selection`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
