<?php
include_once "../../src/GenderSelection/GenderSelection.php";

$showObject = new GenderSelection();

$onedata = $showObject->prepare($_GET)->show();

if (isset($onedata) && !empty($onedata)) {
    ?>
    <html>
        <head>
            <title>Edit | Show</title>
        </head>
        <body>
            <a href="create.php">Back to add page</a>
            <a href="index.php">Back to list page</a>
            <table border="1">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Gender</th>
                    <th>unique_id</th>
                </tr>
                <td><?php echo $onedata['id'] ?></td>
                <td><?php echo $onedata['title'] ?></td>
                <td><?php echo $onedata['gender'] ?></td>
                <td><?php echo $onedata['unique_id'] ?></td>
            </table>

        </body>
    </html>

<?php
} else {
    $_SESSION["Err_mess"] = "not Found 4564675+4";
    header("location:error.php");
} 
?>