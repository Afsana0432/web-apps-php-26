<?php
include_once "../../../../src/BITM/seip06/Actor/Actor.php";
?>
<a href="../../../../views/BITM/seip06/Actor/index.php">List of Actor</a>
<a href="create.php">Add New Actor</a>
<?php
$obj = new Actor();
$Alldata = $obj->index();

if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>
<html>
<head>
    <title>Index | Data</title>
</head>
<body>
<table border="1">
    <tr>
        <th>SL</th>
        <th>Title</th>
        <th colspan="3">Action</th>
    </tr>
    <?php
    $serial = 1;
    if (isset($Alldata) && !empty($Alldata)) {

        foreach ($Alldata as $Singledata) {
            ?>

            <tr>
                <td><?php echo $serial++ ?></td>
                <td><?php echo $Singledata['title'] ?></td>
                <td><a href="show.php?id=<?php echo $Singledata['id'] ?>">View</a></td>
                <td><a href="edit.php?id=<?php echo $Singledata['id'] ?>">Edit</a></td>
                <td><a href="delete.php?id=<?php echo $Singledata['id'] ?>">Delete</a></td>

            </tr>
        <?php }
    } else {
        ?>
        <tr>
            <td colspan="3">
                No available data
            </td>
        </tr>
    <?php } ?>
</table>
</body>
</html>
