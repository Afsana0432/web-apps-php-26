<?php
include_once '../../src/gender/gender.php';
$obj=new gender();
$data =$obj->index();


?>
<html>
<head>
    <title>Viewing Data</title>

    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
</head>
<body>
    <div class="container">
        <div class="col-sm-7">
<a href="create.php">Add Another One</a>
<?php
if(isset($_SESSION['message']) && !empty($_SESSION['message'])){
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}
?>
<table class="table table-hover">
    <tr>
        <th>Name</th>
        <th colspan="3">Action</th>
    </tr>
    
        <?php
        foreach ($data as $value) {
        ?>
    <tr>
    
    <td><?php echo $value['name'];?></td>
    <td><a href="show.php?id=<?php echo $value['id']; ?>">View</a></td>
    <td><a href="edit.php?id=<?php echo $value['id']; ?>">Edit</a></td>
    <td><a href="delete.php?id=<?php echo $value['id']; ?>">Delete</a></td>
    </tr>
    <?php } ?>


</table>

</div>
</div>
</body>
</html>