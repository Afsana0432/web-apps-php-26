<?php
include_once '../../src/gender/gender.php';
$obj=new gender();
$data =$obj->prepare($_GET)->show();

?>
<html>
<head>
  	<title>Specific views</title>
  	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<table class="table table-hover">
						<tr>
							<th>Id</th>
							<th>Name</th>
							<th>Sex</th>
						</tr>
						<tr>
							<td><?php echo $data['id']; ?></td>
							<td><?php echo $data['name']; ?></td>
							<td><?php echo $data['gender']; ?></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>