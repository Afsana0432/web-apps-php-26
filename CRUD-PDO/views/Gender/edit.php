<?php
include_once '../../src/gender/gender.php';
$obj=new gender();
$data =$obj->prepare($_GET)->show();
?>
<html>
 <head>
 	<title>Editing</title>
 	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
 </head>
 <body>
 	<div class="container">
 		<div class="row">
 			<div class=col-sm-5>
 				<h1>Edit Your Individual Item</h1>
 				<form action="update.php" method="POST">
 					<div class=form-group>
 						<input type="hidden" class="form-control" value="<?php echo $data['id'] ?>" name="id">
 					</div>
 					<div class=form-group>
 						<input type="text" class="form-control" value="<?php echo $data['name'] ?>" name="name">
 					</div>
 					<div>
 						<input type="radio" value="<?php echo $data['gender'] ?>" name="gender">male
 						<input type="radio" value="<?php echo $data['gender'] ?>" name="gender">female
 					</div>
 					<div class=form-group>
 						<input type="submit" value="Edit" class="btn btn-primary">
 					</div>

 				</form>
 			</div>
 		</div>
 	</div>
 </body>
<html>