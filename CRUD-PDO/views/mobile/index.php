<?php
//include_once '../../src/mobile/mobile.php';
include_once '../../vendor/autoload.php';

use App\mobile\mobile;

$new = new mobile();
$data = $new->index();
//echo "<pre>";
//print_r($data);
?>
<html>
    <head>
        <title>Mobile Index Page</title>
        <link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
    </head>
    <body>
        <div class="container" style="background-color: #e3e3e3">
            <a href="create.php">Add another one</a>
            <?php
            if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
                echo $_SESSION['Message'];
                unset($_SESSION['Message']);
            }
            ?>
            <table class="table table-hover" border="2">
                <tr>
                    <th>Id</th>
                    <th colspan="3">action</th>
                </tr>
                <?php
                if (isset($data) && !empty($data)) {

                    foreach ($data as $list) {
                         ?>
                        <tr>

                            <td><?php echo $list['title']; ?></td>
                            <td><a href="show.php?id=<?php echo $list['id']; ?>">View</a></td>
                            <td><a href="edit.php?id=<?php echo $list['id']; ?>">Edit</a></td>
                            <td><a href="delete.php?id=<?php echo $list['id']; ?>">Delete</a></td>

                        </tr>
                    <?php }
                } ?>
            </table>
        </div>
    </body>
</html>