<?php
//include_once '../../src/mobile/mobile.php';
include_once '../../vendor/autoload.php';
use App\mobile\mobile;

$myobj = new mobile();
$data = $myobj->prepare($_GET)->show();
?>
<html>
    <head>
        <title>Single view</title>
<!--        <link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">-->
    </head>
    <body>
        <div class="container" style="background-color: pink">
            <a href="index.php">Go to index</a>
         <table class="table table-bordered table-hover">
                <tr>
                    <th>Id</th>
                    <th>Title</th>
                </tr>
                <tr>
                    <td><?php echo $data['id']; ?></td>
                    <td><?php echo $data['title']; ?></td>

                </tr>
            </table>
            
        </div>
    </body>
</html>



