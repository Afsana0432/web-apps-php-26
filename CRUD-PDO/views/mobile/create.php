<?php
session_start();
?>
<!doctype html>
<html>
    <head>
        <title>Favourite Mobile model</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
    </head>
    <body>
        <div class="container">
            <div class="col-sm-5">
                <h2>Favourite mobile model's</h2>
                <a href="index.php">Home Mobile</a>
                <fieldset>
                    <legend>Favourite mobile</legend>
                    <form action="store.php" method="post">
                        <?php
                        if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
                            echo $_SESSION['Message'];
                            unset($_SESSION['Message']);
                        }
                        ?>

                        <div class="form-group">
                            <label>Enter Your Favourite mobile model's</label>
                            <input type="text" class="form-control" name="mobile_model">
                        </div>
                        <div class="form-group">
                            <input type="submit" value="add" class="btn btn-primary">
                        </div>
                    </form>
                </fieldset>
            </div>
        </div>
    </body>
</html>

