<?php
//include_once '../../src/mobile/mobile.php';
include_once '../../vendor/autoload.php';
use App\mobile\mobile;

$myobj = new mobile();
$data = $myobj->prepare($_GET)->show();
?>
<html>
    <head>
        <title>Edit</title>
        <link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
    </head>
    <body>
        
        <div class="container" style="background-color: #e6e6e6">
            <h1>Edit Your Item</h1>
            <div class="row">
                <div class="col-sm-5">
                     <form action="update.php" method="post">
                        <div class="form-group">
                            <label class="hidden">ID</label>
                            <input type="text" class="form-control hidden" name="upid" value="<?php echo $data['id']; ?>">

                        </div>
                         <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control" name="update" value="<?php echo $data['title']; ?>">

                        </div>
                        <div class="form-group">
                            <input type="submit" value="Update" class="btn btn-group">
                        </div>
                    </form>
                </div>
            </div>
    </div>
</body>
</html>



