<?php

namespace App\mobile;

use PDO;

class mobile {

    public $id = '';
    public $title = '';
    public $data1 = '';
    public $show = '';
    public $up = '';
    public $upid = '';
    public $us = 'root';
    public $pw = '';
    public $conn = '';

    public function __construct() {
        session_start();
        $this->conn = new PDO('mysql:host=localhost;dbname=webappsroject', $this->us, $this->pw);
    }

    public function prepare($data = '') {
        if (array_key_exists('mobile_model', $data)) {
            $this->title = $data['mobile_model'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('update', $data)) {
            $this->up = $data['update'];
        }
        if (array_key_exists('upid', $data)) {
            $this->upid = $data['upid'];
        }
        return $this;
    }

    public function store() {
        try {
            $query = "INSERT INTO mobiles (id,title) VALUES (:i,:tit)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':i' => null,
                ':tit' => $this->title
            ));

            // $_SESSION['Message'] = "<h1>Mobile model succesfully saved.</h1>";
            echo '<script>alert("succesfully")</script>';
            echo '<script>window.location.replace("index.php")</script>';
            // header('location:index.php');
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function index() {
        try {
            $q = "SELECT * FROM mobiles";
            $sth = $this->conn->prepare($q);
            $sth->execute();
            while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
                $this->data1[] = $row;
            }
            return $this->data1;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function show() {
        try {
            $qr = "SELECT * FROM `mobiles` WHERE id=" . $this->id;
            $smt = $this->conn->query($qr);
            $smt->execute();
            $row = $smt->fetch(PDO::FETCH_ASSOC);
            return $row;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

        return $this;
    }

    public function delete() {
        /* $query = "DELETE FROM `mobile` WHERE `mobile`.`id` =" . $this->id;
          mysql_query($query);
          $_SESSION['Message'] = "<h1>Mobile model succesfully Deleted.</h1>";

          header('location:index.php'); */
        try {
            $query = "DELETE FROM `mobiles` WHERE `mobiles`.`id` =" . $this->id;
            $s = $this->conn->query($query);
            $s->execute();
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

        $_SESSION['Message'] = "<h1>Mobile model succesfully Deleted.</h1>";
        header('location:index.php');
    }

    public function update() {

        echo $this->up;
        echo $this->upid;
        try {
            $query = "update mobiles SET title='$this->up' WHERE id=" . $this->upid;
//        echo $query;

            $st = $this->conn->query($query);
            $st->execute();

            $_SESSION['Message'] = "<h1>Mobile model succesfully updated.</h1>";
            header('location:index.php');
            // mysql_query($query);
            // $_SESSION['Message'] = "<h1>Mobile model succesfully Updated.</h1>";
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
// header('location:index.php');
    }

}
