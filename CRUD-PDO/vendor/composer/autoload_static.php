<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit37c8f68bb4ff5de81ce9e1c6acc1f16d
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/Src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit37c8f68bb4ff5de81ce9e1c6acc1f16d::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit37c8f68bb4ff5de81ce9e1c6acc1f16d::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
